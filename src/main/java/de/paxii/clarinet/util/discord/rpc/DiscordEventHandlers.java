package de.paxii.clarinet.util.discord.rpc;

public class DiscordEventHandlers {

    public net.arikia.dev.drpc.DiscordEventHandlers getEventHandlers() {
        return new net.arikia.dev.drpc.DiscordEventHandlers.Builder()
                .build();
    }

}
