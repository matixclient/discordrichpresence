package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.LoadWorldEvent;
import de.paxii.clarinet.event.events.gui.DisplayGuiScreenEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.discord.rpc.DiscordEventHandlers;

import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

public class ModuleDiscordRichPresence extends Module {

  private static final String DISCORD_APPLICATION = "482280632504811551";

  private DiscordEventHandlers discordEventHandlers;

  public ModuleDiscordRichPresence() {
    super("DiscordRichPresence", ModuleCategory.OTHER);

    this.discordEventHandlers = new DiscordEventHandlers();

    this.setEnabled(true);
    this.setDisplayedInGui(false);
    this.setRegistered(true);
  }

  @Override
  public void onEnable() {
    DiscordRPC.discordInitialize(
            ModuleDiscordRichPresence.DISCORD_APPLICATION,
            this.discordEventHandlers.getEventHandlers(),
            true
    );

    this.setRichPresence("In Menus", "");
  }

  @EventHandler
  public void onDisplayGuiScreen(DisplayGuiScreenEvent screenEvent) {
    if (Wrapper.getWorld() == null) {
      this.setRichPresence(
              screenEvent.getGuiScreen().getClass().getSimpleName().replaceAll("Gui(Screen)?", "Gui "),
              "In Menus"
      );
    }
  }

  @EventHandler
  public void onLoadWord(LoadWorldEvent loadWorldEvent) {
    if (Wrapper.getMinecraft().getCurrentServerData() != null) {
      this.setRichPresence(
              Wrapper.getMinecraft().getCurrentServerData().serverIP,
              "Playing Online"
      );
    } else if (Wrapper.getMinecraft().getIntegratedServer() != null) {
      this.setRichPresence(
              "World " + Wrapper.getMinecraft().getIntegratedServer().getWorldName(),
              "Playing Offline"
      );
    }
  }

  @Override
  public void onDisable() {
    DiscordRPC.discordShutdown();
  }

  protected void setRichPresence(String state, String details) {
    DiscordRichPresence discordRichPresence = new DiscordRichPresence
            .Builder(state)
            .setDetails(details)
            .build();
    DiscordRPC.discordUpdatePresence(discordRichPresence);
  }
}
